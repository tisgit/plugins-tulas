<?php

/* Shortcode handler */

add_action( 'wpcf7_init', 'wpcf7_add_shortcode_course' );

function wpcf7_add_shortcode_course() {
	wpcf7_add_shortcode( array( 'course', 'course*' ),
		'wpcf7_course_shortcode_handler', true );
}

function wpcf7_course_shortcode_handler( $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) )
		return '';

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type );

	if ( $validation_error )
		$class .= ' wpcf7-not-valid';

	$atts = array();

	$atts['class'] = 'wpcf7-select '.$tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );

	if ( $tag->is_required() )
		$atts['aria-required'] = 'true';

	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';

	$multiple = $tag->has_option( 'multiple' );
	
	$courses = array(
					''				=>	'Select Course',
					'btech-cse' 	=> 	'B.Tech - CSE', 
					'btech-ece'		=> 	'B.Tech - ECE', 
					'btech-eee'		=> 	'B.Tech - EEE', 
					'btech-civil'		=> 	'B.Tech - Civil', 
					'btech-mech'		=> 	'B.Tech - ME', 
'btech-lateral-cse' => 'B.Tech Lateral CSE',
'btech-lateral-ece' => 'B.Tech Lateral ECE',
'btech-lateral-eee' => 'B.Tech Lateral EEE',
'btech-lateral-civil' => 'B.Tech Lateral CE',
'btech-lateral-mech' => 'B.Tech Lateral ME',
					'mtech-cse' 		=> 	'M.Tech - CS', 
					'mtech-Civil' 		=> 	'M.Tech - Civil',					
					'mtech-thermal' => 	'M.Tech - Thermal', 
					'BBA'			=> 	'BBA', 
					'BCA'			=> 	'BCA', 
					'MBA'			=> 	'MBA', 
					'MCA'			=> 	'MCA', 
					'MCA-Lateral'	=> 	'MCA - Lateral', 
					'diploma-eee'	=>	'Diploma - EEE',
					'diploma-mech'	=>	'Diploma - ME',
'diploma-le-eee' => 'Diploma - Lateral EEE',
'diploma-le-mech' => 'Diploma - Lateral ME',
					'Bsc-agriculture'	=> 	'B.Sc Agriculture',
					'Bsc-forestry'	=> 	'B.Sc Forestry',
					'B.Com Hons.' => 'B.Com (Honours)',
					'BJMC' => 'Mass Communication',
				);
				
	$options = $courses;
	

/*	$defaults = array();

	$default_choice = $tag->get_default_option( null, 'multiple=1' );

	foreach ( $default_choice as $value ) {
		$key = array_search( $value, $values, true );

		if ( false !== $key ) {
			$defaults[] = (int) $key + 1;
		}
	}

	if ( $matches = $tag->get_first_match_option( '/^default:([0-9_]+)$/' ) ) {
		$defaults = array_merge( $defaults, explode( '_', $matches[1] ) );
	}

	$defaults = array_unique( $defaults );*/


	$html = '';
	$hangover = wpcf7_get_hangover( $tag->name );

	foreach ( $options as $key => $value ) {
		$selected = false;

		if ( $hangover ) {
			if ( $multiple ) {
				$selected = in_array( esc_sql( $key ), (array) $hangover );
			} else {
				$selected = ( $hangover == esc_sql( $key ) );
			}
		} /*else {
			if ( ! $shifted && in_array( (int) $key + 1, (array) $defaults ) ) {
				$selected = true;
			} elseif ( $shifted && in_array( (int) $key, (array) $defaults ) ) {
				$selected = true;
			}
		}
*/
		$item_atts = array(
			'value' => $key,
			'selected' => $selected ? 'selected' : '' );

		$item_atts = wpcf7_format_atts( $item_atts );

		$label = $value;

		$html .= sprintf( '<option %1$s>%2$s</option>',
			$item_atts, esc_html( $label ) );
	}

	if ( $multiple )
		$atts['multiple'] = 'multiple';

	$atts['name'] = $tag->name . ( $multiple ? '[]' : '' );

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><select id="course" %2$s>%3$s</select>%4$s</span>',
		sanitize_html_class( $tag->name ), $atts, $html, $validation_error );

	return $html;
}


/* Validation filter */

add_filter( 'wpcf7_validate_course', 'wpcf7_course_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_course*', 'wpcf7_course_validation_filter', 10, 2 );

function wpcf7_course_validation_filter( $result, $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	$name = $tag->name;

	if ( isset( $_POST[$name] ) && is_array( $_POST[$name] ) ) {
		foreach ( $_POST[$name] as $key => $value ) {
			if ( '' === $value )
				unset( $_POST[$name][$key] );
		}
	}

	$empty = ! isset( $_POST[$name] ) || empty( $_POST[$name] ) && '0' !== $_POST[$name];

	if ( $tag->is_required() && $empty ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	}

	return $result;
}


/* Tag generator */

add_action( 'wpcf7_admin_init', 'wpcf7_add_tag_generator_course', 25 );

function wpcf7_add_tag_generator_course() {
	$tag_generator = WPCF7_TagGenerator::get_instance();
	$tag_generator->add( 'course', __( 'course dropdown', 'contact-form-7' ),
		'wpcf7_tag_generator_course' );
}

function wpcf7_tag_generator_course( $contact_form, $args = '' ) {
	$args = wp_parse_args( $args, array() );

	$description = __( "Generate a form-tag for a course drop-down. For more details, see %s.", 'contact-form-7' );

	$desc_link = wpcf7_link( __( 'http://contactform7.com/checkboxes-radio-buttons-and-menus/', 'contact-form-7' ), __( 'Checkboxes, Radio Buttons and Menus', 'contact-form-7' ) );

?>
<div class="control-box">
<fieldset>
<legend><?php echo sprintf( esc_html( $description ), $desc_link ); ?></legend>

<table class="form-table">
<tbody>
	<tr>
	<th scope="row"><?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?></th>
	<td>
		<fieldset>
		<legend class="screen-reader-text"><?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?></legend>
		<label><input type="checkbox" name="required" /> <?php echo esc_html( __( 'Required field', 'contact-form-7' ) ); ?></label>
		</fieldset>
	</td>
	</tr>

	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-name' ); ?>"><?php echo esc_html( __( 'Name', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="name" class="tg-name oneline" id="<?php echo esc_attr( $args['content'] . '-name' ); ?>" /></td>
	</tr>

	<tr>
	<th scope="row"><?php echo esc_html( __( 'Options', 'contact-form-7' ) ); ?></th>
	<td>
		<fieldset>
		<legend class="screen-reader-text"><?php echo esc_html( __( 'Options', 'contact-form-7' ) ); ?></legend>
		<label><input type="checkbox" name="multiple" class="option" /> <?php echo esc_html( __( 'Allow multiple selections', 'contact-form-7' ) ); ?></label><br />
		</fieldset>
	</td>
	</tr>

	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-id' ); ?>"><?php echo esc_html( __( 'Id attribute', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="id" class="idvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-id' ); ?>" /></td>
	</tr>

	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-class' ); ?>"><?php echo esc_html( __( 'Class attribute', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="class" class="classvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-class' ); ?>" /></td>
	</tr>

</tbody>
</table>
</fieldset>
</div>

<div class="insert-box">
	<input type="text" name="course" class="tag code" readonly="readonly" onfocus="this.select()" />

	<div class="submitbox">
	<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
	</div>

	<br class="clear" />

	<p class="description mail-tag"><label for="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>"><?php echo sprintf( esc_html( __( "To use the value input through this field in a mail field, you need to insert the corresponding mail-tag (%s) into the field on the Mail tab.", 'contact-form-7' ) ), '<strong><span class="mail-tag"></span></strong>' ); ?><input type="text" class="mail-tag code hidden" readonly="readonly" id="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>" /></label></p>
</div>
<?php
}
