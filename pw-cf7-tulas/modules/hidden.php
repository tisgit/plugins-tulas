<?php

/* Shortcode handler */

add_action( 'wpcf7_init', 'wpcf7_add_shortcode_hidden' );

function wpcf7_add_shortcode_hidden() {
	wpcf7_add_shortcode( array( 'hidden' ),
		'wpcf7_hidden_shortcode_handler', true );
}

function wpcf7_hidden_shortcode_handler( $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) )
		return '';

	$atts = array();

	$atts['value'] = $tag->get_option( 'value', '', true );
	$atts['type'] = 'hidden';

	$html = '';
	$hangover = wpcf7_get_hangover( $tag->name );

	$atts['name'] = $tag->name;

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf('<input %1$s />', $atts);

	return $html;
}


/* Validation filter */

add_filter( 'wpcf7_validate_hidden', 'wpcf7_hidden_validation_filter', 10, 2 );

function wpcf7_hidden_validation_filter( $result, $tag ) {

	return $result;
}


/* Tag generator */

add_action( 'wpcf7_admin_init', 'wpcf7_add_tag_generator_hidden', 25 );

function wpcf7_add_tag_generator_hidden() {
	$tag_generator = WPCF7_TagGenerator::get_instance();
	$tag_generator->add( 'hidden', __( 'hidden field', 'contact-form-7' ),
		'wpcf7_tag_generator_hidden' );
}

function wpcf7_tag_generator_hidden( $contact_form, $args = '' ) {
	$args = wp_parse_args( $args, array() );

	$description = __( "Generate a form-tag for a hidden field. For more details, see %s.", 'contact-form-7' );

	$desc_link = wpcf7_link( __( 'http://contactform7.com/checkboxes-radio-buttons-and-menus/', 'contact-form-7' ), __( 'Checkboxes, Radio Buttons and Menus', 'contact-form-7' ) );

?>
<div class="control-box">
<fieldset>
<legend><?php echo sprintf( esc_html( $description ), $desc_link ); ?></legend>

<table class="form-table">
<tbody>
	<tr>
	<th scope="row"><?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?></th>
	
	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-name' ); ?>"><?php echo esc_html( __( 'Name', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="name" class="tg-name oneline" id="<?php echo esc_attr( $args['content'] . '-name' ); ?>" /></td>
	</tr>

	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-value' ); ?>"><?php echo esc_html( __( 'Value', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="value" class="idvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-value' ); ?>" /></td>
	</tr>

</tbody>
</table>
</fieldset>
</div>

<div class="insert-box">
	<input type="text" name="hidden" class="tag code" readonly="readonly" onfocus="this.select()" />

	<div class="submitbox">
	<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
	</div>

	<br class="clear" />

	<p class="description mail-tag"><label for="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>"><?php echo sprintf( esc_html( __( "To use the value input through this field in a mail field, you need to insert the corresponding mail-tag (%s) into the field on the Mail tab.", 'contact-form-7' ) ), '<strong><span class="mail-tag"></span></strong>' ); ?><input type="text" class="mail-tag code hidden" readonly="readonly" id="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>" /></label></p>
</div>
<?php
}
