jQuery(document).ready( function($) {
	
	$('select#state').change(function() {
		var sel=$('select#city');
		sel.empty().append('<option></option>');
		$.ajax({
			type:"POST",
			url: pw_ajaxurl ,
			data: {'action': 'city_selection', 'id' : $(this).val()},
			success:function(data){
			//	console.log(data);
				for(i=0;i<data.length;i++) {
				//	console.log(data[i]);
					sel.append('<option value="' + data[i].id + '"'+'>' + data[i].name+'</option>');
				}
				sel.append('<option value="-1">Other</option>');
			}
		});
		
	});
	
})