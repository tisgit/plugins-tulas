<?php
/*
Plugin Name: Contact Form 7 Extention - Tula's
Plugin URI: 
Description: Contact Form 7 Extention for Tula's Institute
Author: PinnacleWorks
Author URI: 
Text Domain: contact-form-7
Version: 1.0
*/

define( 'PWCF7_PLUGIN', __FILE__ );
define( 'PWCF7_PLUGIN_BASENAME', plugin_basename( PWCF7_PLUGIN ) );
define( 'PWCF7_PLUGIN_NAME', trim( dirname( PWCF7_PLUGIN_BASENAME ), '/' ) );
define( 'PWCF7_PLUGIN_DIR', untrailingslashit( dirname( PWCF7_PLUGIN ) ) );
define( 'PWCF7_PLUGIN_MODULES_DIR', PWCF7_PLUGIN_DIR . '/modules' );
define( 'PWCF7_PLUGIN_JSON_DIR', PWCF7_PLUGIN_DIR . '/json' );
define( 'PWCF7_PLUGIN_JSON_FILE', file_get_contents(PWCF7_PLUGIN_JSON_DIR.'/json_data.txt',true));
include(PWCF7_PLUGIN_MODULES_DIR.'/course.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/course_bsc.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/course_computer_application.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/course_engineering.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/course_mgmt.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/course_others.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/state.php');
//include(PWCF7_PLUGIN_MODULES_DIR.'/city.php');
//include(PWCF7_PLUGIN_MODULES_DIR.'/test.php');
include(PWCF7_PLUGIN_MODULES_DIR.'/hidden.php');


function pwcf7_get_data($type) {
	
	$jsondata = (array)json_decode(PWCF7_PLUGIN_JSON_FILE);
	return $jsondata[$type];
}


add_action( 'wp_head', 'pwcf7_set_ajax_url' );
function pwcf7_set_ajax_url() { 
	echo '<script type="text/javascript" >pw_ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';
}

add_action( 'wp_ajax_city_selection', 'pwcf7_city_selection_callback' );
add_action( 'wp_ajax_nopriv_city_selection', 'pwcf7_city_selection_callback' );
/*
register_activation_hook(__FILE__, 'pwcf7_activation');
add_action('my_hourly_event', 'pwcf7_get_json_data');

function pwcf7_activation() {
	wp_schedule_event(time(), 'hourly', 'my_hourly_event');
}

function pwcf7_get_json_data() {
	
	$homepage = file_get_contents('http://leads.tulasinternational.edu.in/json_data');
	$myfile = fopen(PWCF7_PLUGIN_JSON_DIR."/json_data.txt", "w") or die("Unable to open file!");
	fwrite($myfile, $homepage);
	fclose($myfile);
}

register_deactivation_hook(__FILE__, 'pwcf7_deactivation');

function pwcf7_deactivation() {
	wp_clear_scheduled_hook('my_hourly_event');
}
*/
function pwcf7_city_selection_callback() {
	
	$state_id = $_POST['id'];
	$cities = pwcf7_get_data('cities');

		for($i=0;$i<count($cities);$i++){
			$temp = (array)$cities[$i];
			if($state_id == $temp['state_id']){
				$city['id'] = $temp['id'];
				$city['name'] = $temp['name'];
				$temp1[] = $city;
			}
		}
		header('Content-Type: text/json');
		echo json_encode($temp1);
	die();
}