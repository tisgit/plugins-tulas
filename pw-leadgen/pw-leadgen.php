<?php

/*
Plugin Name: Lead Generation - Tula's
Author: PinnacleWorks
Version: 1.0
*/


define( 'PWLG_PLUGIN', __FILE__ );
define( 'PWLG_PLUGIN_BASENAME', plugin_basename( PWLG_PLUGIN ) );
define( 'PWLG_PLUGIN_NAME', trim( dirname( PWLG_PLUGIN_BASENAME ), '/' ) );
define( 'PWLG_PLUGIN_DIR', untrailingslashit( dirname( PWLG_PLUGIN ) ) );

include(PWLG_PLUGIN_DIR.'/post_lead_data.php');
include(PWLG_PLUGIN_DIR.'/otp.php');


# validation for mobile numbers
function cf7_custom_tel_validation($result, $tag) {
	
	$tag = new WPCF7_Shortcode( $tag );
	
	$name = $tag->name;
	if ($name == 'mobile' || $name == 'phone') {
		$mobile = isset( $_POST[$name] ) ? wpcf7_canonicalize( $_POST[$name] ) : '';
		$country_code = isset( $_POST['country-code'] ) ? wpcf7_canonicalize( $_POST['country-code'] ) : '';
		
		if($mobile != '') {
			if ($country_code == '+91') {
				if(strlen($mobile) != 10) {				
					$result->invalidate( $tag, 'Mobile No. should be 10 digits' );
				}
			} else {
				if(strlen($mobile) < 7) {				
					$result->invalidate( $tag, 'Mobile No. should be atleast 7 digits' );
				} elseif(strlen($mobile) > 12) {				
					$result->invalidate( $tag, 'Mobile No. should not be more than 12 digits' );
				}
			}			
		}
	}
	
	return $result;
}

add_filter( 'wpcf7_validate_tel*', 'cf7_custom_tel_validation', 10, 2 );