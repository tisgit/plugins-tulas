<?php
/**
** A base module for [otp]
**/

/* Shortcode handler */

add_action( 'wpcf7_init', 'wpcf7_add_shortcode_otp' );

function wpcf7_add_shortcode_otp() {
	wpcf7_add_shortcode( 'otp', 'wpcf7_otp_shortcode_handler', true );
}

function wpcf7_otp_shortcode_handler( $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) )
		return '';

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type );

	if ( $validation_error )
		$class .= ' wpcf7-not-valid';

	$atts = array();

	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );
	$atts['aria-required'] = 'true';
	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';

	$atts['type'] = 'text';
	$atts['name'] = $tag->name;
	$atts['placeholder'] = 'Enter OTP';

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><input %2$s />%3$s</span>',
		sanitize_html_class( $tag->name ), $atts, $validation_error );
		
	return $html;
}


/* Validation filter */

add_filter( 'wpcf7_validate_otp', 'wpcf7_otp_validation_filter', 10, 2 );

function wpcf7_otp_validation_filter( $result, $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	$name = $tag->name;

	$otp = isset( $_POST[$name] ) ? wpcf7_canonicalize( $_POST[$name] ) : '';

	$posted_data = get_transient($_SERVER['REMOTE_ADDR'].'_lead_form');
		
	if (isset($posted_data['country_code']) && isset($posted_data['phone'])) {
		
		$base_url = 'http://lms.pinnacleworks.net/';
		
		$form_fields = array();
		$form_fields['country_code'] = $posted_data['country_code'];
		$form_fields['phone'] = $posted_data['phone'];	
		$form_fields['otp'] = $otp;
		$form_fields['request'] = 'remote/verify_mobile';
		$form_fields['account'] = 'ti';
	
			
		$url = $base_url;
		$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($form_fields));
		$ch_result=curl_exec($ch);
	//	var_dump($ch_result);
		
		if ($ch_result != '1') {
			$result->invalidate( $tag, wpcf7_get_message( 'otp_not_correct' ) );
		}
	}
	
	return $result;
}


/* Messages */

add_filter( 'wpcf7_messages', 'wpcf7_otp_messages' );

function wpcf7_otp_messages( $messages ) {
	return array_merge( $messages, array( 'otp_not_correct' => array(
		'description' => __( "Sender doesn't enter the correct OTP", 'contact-form-7' ),
		'default' => __( 'OTP incorrect.', 'contact-form-7' )
	) ) );
}


/* Tag generator */

add_action( 'wpcf7_admin_init', 'wpcf7_add_tag_generator_otp', 40 );

function wpcf7_add_tag_generator_otp() {
	$tag_generator = WPCF7_TagGenerator::get_instance();
	$tag_generator->add( 'otp', __( 'otp', 'contact-form-7' ),
		'wpcf7_tag_generator_otp' );
}

function wpcf7_tag_generator_otp( $contact_form, $args = '' ) {
	$args = wp_parse_args( $args, array() );
	$type = 'otp';

	$description = __( "Generate a form-tag for a question-answer pair. For more details, see %s.", 'contact-form-7' );

	$desc_link = wpcf7_link( __( 'http://contactform7.com/otp/', 'contact-form-7' ), __( 'Quiz', 'contact-form-7' ) );

?>
<div class="control-box">
<fieldset>
<legend><?php echo sprintf( esc_html( $description ), $desc_link ); ?></legend>
<table class="form-table">
<tbody>
<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-name' ); ?>"><?php echo esc_html( __( 'Name', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="name" class="tg-name oneline" id="<?php echo esc_attr( $args['content'] . '-name' ); ?>" /></td>
	</tr>
	
	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-id' ); ?>"><?php echo esc_html( __( 'Id attribute', 'contact-form-7' ) ); ?></label></th>
	<td><input type="text" name="id" class="idvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-id' ); ?>" /></td>
	</tr>
	</tbody>
</table>
</fieldset>
</div>

<div class="insert-box">
	<input type="text" name="<?php echo $type; ?>" class="tag code" readonly="readonly" onfocus="this.select()" />

	<div class="submitbox">
	<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
	</div>
</div>
<?php
}
