<?php

add_filter( 'wpcf7_posted_data', 'pwlg_verify_mobile' );
function pwlg_verify_mobile( $posted_data ) {
	
	$base_url = 'http://leads.tulasinternational.edu.in/';
	$form_fields = array();
		
	if (isset($posted_data['otp'])) {
		
		$posted_data = get_transient($_SERVER['REMOTE_ADDR'].'_lead_form');
		
		if (isset($posted_data['country_code']) && isset($posted_data['phone'])) {
			
			$form_fields['country_code'] = $posted_data['country_code'];
			$form_fields['phone'] = $posted_data['phone'];	
		

		//	print_r($form_fields);
			
			$url = $base_url.'remote/verify_mobile';
			$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($form_fields));
			$result=curl_exec($ch);
		}
	}
	
	return $posted_data;
}